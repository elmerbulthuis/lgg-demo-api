import { HttpSendReceive } from "@oas3/http-send-receive";
import * as prom from "prom-client";

export interface Config {
    endpoint: URL;
    clientApiEndpoint: URL;
    clientId: string;
    clientSecret: string;

    abortController: AbortController;
    promRegistry: prom.Registry;

    httpSendReceive?: HttpSendReceive;
}
