import * as demoApi from "@latency.gg/lgg-demo-oas";
import * as oas3ts from "@oas3/oas3ts-lib";
import * as operations from "../operations/index.js";
import { Context } from "./context.js";

export type Server = demoApi.Server;

export function createServer(
    context: Context,
    onError: (error: unknown) => void,
) {
    const server = new demoApi.Server({
        baseUrl: context.config.endpoint,
    });

    server.registerMiddleware(oas3ts.createErrorMiddleware(onError));

    server.registerMiddleware(
        async function (this, route, request, next) {
            const operation = route ?
                oas3ts.getOperationId(demoApi.metadata, route.name, request.method) :
                undefined;
            const stopTimer = context.metrics.operationDuration.startTimer({ operation });
            try {
                const response = await next(request);
                stopTimer({ status: response.status });
                return response;
            }
            catch (error) {
                stopTimer();

                throw error;
            }
        },
    );

    server.registerGetMetricsOperation(
        operations.createGetMetricsOperation(context),
    );

    return server;
}
