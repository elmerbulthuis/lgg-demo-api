import * as clientApi from "@latency.gg/lgg-client-oas";
import { createRedirectMiddleware } from "@oas3/oas3ts-lib";
import { Config } from "./config.js";

export interface Services {
    client: clientApi.Client;
}

export function createServices(
    config: Config,
): Services {
    const client = new clientApi.Client(
        {
            baseUrl: config.clientApiEndpoint,
            httpSendReceive: config.httpSendReceive,
        },
        {
            basic: {
                username: config.clientId,
                password: config.clientSecret,
            },
        },
    );
    client.registerMiddleware(
        createRedirectMiddleware(),
    );

    return {
        client,
    };
}
