import * as clientApi from "@latency.gg/lgg-client-oas";
import * as demoApi from "@latency.gg/lgg-demo-oas";
import * as http from "http";
import * as prom from "prom-client";
import { Promisable } from "type-fest";
import * as application from "../application/index.js";

interface Context {
    endpoints: {
        application: URL,
        client: URL,
    },
    servers: {
        application: application.Server,
        client: clientApi.Server,
    },
    services: application.Services,
    createApplicationClient(): demoApi.Client,
}

export async function withContext<T>(
    job: (context: Context) => Promisable<T>,
) {
    const endpoints = {
        application: new URL("http://localhost:8080"),
        client: new URL("http://localhost:9010"),
    };

    const abortController = new AbortController();
    const promRegistry = new prom.Registry();

    const applicationConfig: application.Config = {
        endpoint: endpoints.application,
        clientApiEndpoint: endpoints.client,
        clientId: "x",
        clientSecret: "y",

        abortController,
        promRegistry,
    };

    const onError = (error: unknown) => {
        if (!abortController.signal.aborted) {
            throw error;
        }
    };

    const applicationContext = application.createContext(
        applicationConfig,
    );
    try {

        const servers = {
            application: application.createServer(
                applicationContext,
                onError,
            ),
            client: new clientApi.Server({
                baseUrl: endpoints.client,
            }),
        };

        const httpServers = {
            application: http.createServer(servers.application.asRequestListener({
                onError,
            })),
            client: http.createServer(servers.client.asRequestListener({
                onError,
            })),
        };

        const context = {
            endpoints,
            servers,
            createApplicationClient() {
                return new demoApi.Client(
                    {
                        baseUrl: endpoints.application,
                    },
                    {
                        apiKey: "-",
                        token: "-",
                    },
                );
            },
            services: applicationContext.services,
        };

        const keys = Object.keys(httpServers) as Array<keyof typeof httpServers>;
        await Promise.all(
            keys.map(async key => new Promise<void>(resolve => httpServers[key].listen(
                endpoints[key].port,
                () => resolve(),
            ))),
        );

        try {
            const result = await job(context);
            return result;
        }
        finally {
            abortController.abort();

            await Promise.all(
                keys.map(async key => new Promise<void>(
                    (resolve, reject) => httpServers[key].close(
                        error => error ?
                            reject(error) :
                            resolve(),
                    )),
                ),
            );
        }
    }
    finally {
        await applicationContext.destroy();
    }
}
