import * as demoApi from "@latency.gg/lgg-demo-oas";
import { getSourceIp } from "@oas3/http-tools";
import { getHttpIncomingMessage } from "@oas3/oas3ts-lib";
import assert from "assert";
import * as net from "net";
import * as application from "../application/index.js";

export function createGetMetricsOperation(
    context: application.Context,
): demoApi.GetMetricsOperationHandler {
    return async function operation(
        incomingRequest, authorization, acceptTypes,
    ) {
        const incomingMessage = getHttpIncomingMessage();
        const source = getSourceIp(incomingMessage);

        assert(source);

        const cleanMetrics: demoApi.ProviderSchema = {};
        const staleMetrics: demoApi.ProviderSchema = {};

        const generateIdentResult = await context.services.client.generateIdent({
            parameters: {
                probeIp: source,
            },
        });
        assert(generateIdentResult.status === 202);

        const generateIdentEntity = await generateIdentResult.entity();

        const getProvidersResult = await context.services.client.getSupportedProviders({
            parameters: {},
        });
        assert(getProvidersResult.status === 202);

        const getProvidersEntity = await getProvidersResult.entity();

        for (const provider of getProvidersEntity) {
            staleMetrics[provider.name] = {};
            cleanMetrics[provider.name] = {};

            for (const location of provider.locations) {
                const locationMetricsResult = await context.services.client.getLocationMetrics({
                    parameters: {
                        providerName: provider.name,
                        locationName: location,
                        probeIp: source,
                    },
                });
                assert(locationMetricsResult.status === 202);

                const locationMetricsEntity = await locationMetricsResult.entity();
                if (locationMetricsEntity.stale) {
                    staleMetrics[provider.name][location] = {
                        rtt: locationMetricsEntity.rtt,
                        stddev: locationMetricsEntity.stddev,
                        stale: locationMetricsEntity.stale,
                        beacons: locationMetricsEntity.beacons,
                    };
                }
                else {
                    cleanMetrics[provider.name][location] = {
                        rtt: locationMetricsEntity.rtt,
                        stddev: locationMetricsEntity.stddev,
                        stale: locationMetricsEntity.stale,
                        beacons: locationMetricsEntity.beacons,
                    };
                }
            }
        }

        return {
            parameters: {},
            status: 200,
            entity() {
                return {
                    source: {
                        addr: source,
                        version: net.isIP(source),
                        ident: generateIdentEntity.ident,
                    },
                    clean_metrics: cleanMetrics,
                    stale_metrics: staleMetrics,
                };
            },
        };
    };
}
